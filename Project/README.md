Getting and Cleaning Data: Course Project
=========================================

Introduction
------------
This repository contains 
my work for the course project for the Coursera course "Getting and Cleaning data", part of the Data Science specialization.

About The Raw Data
------------------
[Original Data](https://d396qusza40orc.cloudfront.net/getdata%2Fprojectfiles%2FUCI%20HAR%20Dataset.zip)  
[Original Description](http://archive.ics.uci.edu/ml/datasets/Human+Activity+Recognition+Using+Smartphones)  
Date Accessed: 7/25/2014 6:52:32 AM 

The features (561 of them) are unlabeled and can be found in the **`/train/X_train.txt`**. The activity label numbers are in the **`/train/y_train.txt`** file. The test subjects are in the **`/train/subject_train.txt`** file. The same holds for the test set. The activity label characters can be found in the **`/activity_labels.txt`**. The features labels can be found in the **`/features.txt`**.


About The Script and The `Tidy` Data Set
-------------------------------------

The attached R script **`run_analysis.R`** performs the following to clean up the data:

#####**0.** The reproducible way to download the data file.
* Offers you a reproducible way to check, download, and unzip the necessary data files. Also, it creates a folder called `/data` in the current working directory and changes to working directory to this folder.


#####**1.** Merges the training and the test sets to create one data set. 
* Reads and merges **`/train/X_train.txt`**, **`/train/y_train.txt`**, **`/train/subject_train.txt`** in one step, and creates the `train` set. The same holds for the `test` set.
* Reads **`/activity_labels.txt`**.
* Reads **`/features.txt`**.
* Merges `train` and `test` sets, and creates `all.data` with `10299x563` dimensions. 


#####**2.** Extracts only the measurements on the mean and standard deviation for each measurement.   
* Selects and creates index for the features with `mean` and `standard deviation` for each measurement in the **`/features.txt`** file.
* Subsets the **`/features.txt`** with the created index and creates `features.select`
* Updates the index by adding index numbers for `subject` and `activity` variables at the beginning of the index.
* Subsets the `all.data` with the final index. The result is a `10299x68` data frame. (Note that last 7 measurement names are intentionally left out since the instructions clearly asking to extract only the measurements on the mean and standard deviation for each measurement but not the angle() variables. Also, `meanFreq` type measurements are left out since they are not actual individual measurement means.


#####**3.** Uses descriptive activity names to name the activities in the data set.
* Replaces activity label numbers with activity label characters from the **`/activity_labels.txt`** file in the `all.data`
* Edit activity label characters: converts to lower cases and clears the underscores.
* Coercing activity label characters to factors.

#####**4.** Appropriately labels the data set with descriptive activity names.
* Labels `all.data` column names with activity, subject and selected measurement names from `features.select`.
* Edits the labels.

#####**5.** Creates a second, independent tidy data set with the average of each variable for each activity and each subject.
* Changes the class of subject to numeric to simply aggregate and arrange data in one step.
* Reshapes the `all.data` with the average of each variable for each activity and each subject. the result is a `180x68` data frame.
* Changes the class of subject to factor.
* Writes the `all.data` data frame to `tidy.txt` file in the current directory which is `/data/tidy.txt`.
* Deletes all the unnecessary data frames. 

About the Code Book
-------------------
The CodeBook.md file explains  

- Information about the variables (including units) in the data set not contained in the tidy data.  
- Information about the summary choices.  
- Information about the experimental study design.


About the Dependencies
-------------------
`run_analysis.R` file will help you to install the dependencies automatically. It depends only on `plyr`.