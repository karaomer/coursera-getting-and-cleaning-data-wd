CodeBook for The `Tidy` Data set
=============================

About The Raw Data
------------------
This dataset is derived from the "Human Activity Recognition Using Smartphones Data Set" which was originally made available at [here](https://d396qusza40orc.cloudfront.net/getdata%2Fprojectfiles%2FUCI%20HAR%20Dataset.zip). The original description can be seen [here](http://archive.ics.uci.edu/ml/datasets/Human+Activity+Recognition+Using+Smartphones).  
Date Accessed: 7/25/2014 6:54:42 AM 


For Each Record It is Provided:
-------------------------------
 
- A 66-feature vector with time and frequency domain variables. 
- Its activity label. 
- An identifier of the subject who carried out the experiment. 

Feature Selection in the `Tidy` Data Set
-----------------
Please refer to the `README.txt` and `features.txt` files in the original data set to learn more about the feature selection for this data set. And there you will find the follow description:

> The features selected for this database come from the accelerometer and gyroscope 3-axial raw signals `tAcc-XYZ` and `tGyro-XYZ`. These time domain signals (prefix 't' to denote time) were captured at a constant rate of 50 Hz. Then they were filtered using a median filter and a 3rd order low pass Butterworth filter with a corner frequency of 20 Hz to remove noise. Similarly, the acceleration signal was then separated into body and gravity acceleration signals (tBodyAcc-XYZ and tGravityAcc-XYZ) using another low pass Butterworth filter with a corner frequency of 0.3 Hz.

> Subsequently, the body linear acceleration and angular velocity were derived in time to obtain Jerk signals (tBodyAccJerk-XYZ and tBodyGyroJerk-XYZ). Also the magnitude of these three-dimensional signals were calculated using the Euclidean norm (tBodyAccMag, tGravityAccMag, tBodyAccJerkMag, tBodyGyroMag, tBodyGyroJerkMag). 

> Finally a Fast Fourier Transform (FFT) was applied to some of these signals producing fBodyAcc-XYZ, fBodyAccJerk-XYZ, fBodyGyro-XYZ, fBodyAccJerkMag, fBodyGyroMag, fBodyGyroJerkMag. (Note the 'f' to indicate frequency domain signals).


The reasoning behind selection of features is that the assignment explicitly states "Extracts only the measurements on the mean and standard deviation for each measurement." To be complete, all variables having to do with mean or standard deviation are included. Note that `angle()` type mean measurements and `meanFreq` type measurements are left out since they are not actual individual measurement means.

In short, for this derived data set, these signals were used to estimate variables of the feature vector for each pattern:  
`-XYZ` is used to denote 3-axial signals in the `X`, `Y` and `Z` directions.

* `tBodyAcc-XYZ`
* `tGravityAcc-XYZ`
* `tBodyAccJerk-XYZ`
* `tBodyGyro-XYZ`
* `tBodyGyroJerk-XYZ`
* `tBodyAccMag`
* `tGravityAccMag`
* `tBodyAccJerkMag`
* `tBodyGyroMag`
* `tBodyGyroJerkMag`
* `fBodyAcc-XYZ`
* `fBodyAccJerk-XYZ`
* `fBodyGyro-XYZ`
* `fBodyAccMag`
* `fBodyAccJerkMag`
* `fBodyGyroMag`
* `fBodyGyroJerkMag`

The set of variables that were estimated (and kept for this assignment) from these signals are: 

* `mean()`: Mean value
* `std()`: Standard deviation
  
Other estimates have been removed for the purpose of this excercise.
So, with all the options this `tidy` data set have `66 feature` variables. The resulting variable names are of the following form: tBodyaccMeanX, which means the mean value of tBodyAcc-X.

Note: prefix `t` to denote time and prefix `f` to denote frequency, so the measurements starting with `t` and `f` have units `time` and `frequency` respectively.  

Note: features are normalized and bounded within [-1,1].

Activity Labels in the `Tidy` Data Set 
------------------------------------------
There are 6 different activities for each subject and measurement.
- `laying`
- `sitting`
- `standing`
- `walking`
- `walkingdownstairs`
- `walkingupstairs`

Subject in the `Tidy` Data Set 
------------------------------------------
An identifier of the subject who carried out the experiment. There are `30` subjects. 